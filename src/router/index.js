import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/formInput1',
    name: 'Form Input 1',
    component: () => import('../views/FormInput1.vue')
  },
  {
    path: '/formInput2',
    name: 'Form Input 2',
    component: () => import('../views/FormInput2.vue')
  },
  {
    path: '/formInput3',
    name: 'Form Input 3',
    component: () => import('../views/FormInput3.vue')
  },
  {
    path: '/formInput4',
    name: 'Form Input 4',
    component: () => import('../views/FormInput4.vue')
  },
  {
    path: '/formInput5',
    name: 'Form Input 5',
    component: () => import('../views/FormInput5.vue')
  },
  {
    path: '/formInput6',
    name: 'Form Input 6',
    component: () => import('../views/FormInput6.vue')
  },
  {
    path: '/formInput7',
    name: 'Form Input 7',
    component: () => import('../views/FormInput7.vue')
  },
  {
    path: '/formInput8',
    name: 'Form Input 8',
    component: () => import('../views/FormInput8.vue')
  },
  {
    path: '/formInput9',
    name: 'Form Input 9',
    component: () => import('../views/FormInput9.vue')
  },
  {
    path: '/formm1',
    name: 'Formm 1',
    component: () => import('../views/FormM1.vue')
  },
  {
    path: '/formm2',
    name: 'Formm 2',
    component: () => import('../views/FormM2.vue')
  },
  {
    path: '/formm3',
    name: 'Formm 3',
    component: () => import('../views/FormM3.vue')
  },
  {
    path: '/formm4',
    name: 'Formm 4',
    component: () => import('../views/FormM4.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
